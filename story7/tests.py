from django.test import TestCase, Client, LiveServerTestCase
from .views import index
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager

# Create your tests here.
class Story7UnitTest(TestCase):
    def test_story7_url_is_exist(self):
        response = Client().get('/story7/')
        self.assertEqual(response.status_code, 200)

    def test_story7_using_index_func(self):
        found = resolve('/story7/')
        self.assertEqual(found.func, index)

    def test_story7_using_index_template(self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response, 'story7index.html')

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(ChromeDriverManager().install(), chrome_options=chrome_options)

    def tearDown(self):
        self.browser.quit()

    def test_switch_and_accordion_behaviour(self):
        # test switch can change color
        self.browser.get(self.live_server_url + '/story7/')
        switch = self.browser.find_element_by_id('tombol')
        self.assertIn('class="light"', self.browser.page_source)
        switch.click()
        self.assertNotIn('class="light"', self.browser.page_source)
        self.assertIn('class="dark"', self.browser.page_source)
        switch.click()
        self.assertIn('class="light"', self.browser.page_source)
        self.assertNotIn('class="dark"', self.browser.page_source)

        # test an accordion opens when clicked
        accordion1 = self.browser.find_element_by_id('ui-id-1')
        konten_accordion1 = self.browser.find_element_by_id('ui-id-2')
        self.assertTrue(konten_accordion1.value_of_css_property('display'), 'none')
        accordion1.click()
        self.assertTrue(konten_accordion1.value_of_css_property('display'), 'block')

        # test currently open accordion closes when another is clicked (and opened)
        accordion2 = self.browser.find_element_by_id('ui-id-3')
        konten_accordion2 = self.browser.find_element_by_id('ui-id-3')
        self.assertTrue(konten_accordion2.value_of_css_property('display'), 'none')
        accordion2.click()
        self.assertTrue(konten_accordion2.value_of_css_property('display'), 'block')
        self.assertTrue(konten_accordion1.value_of_css_property('display'), 'none')