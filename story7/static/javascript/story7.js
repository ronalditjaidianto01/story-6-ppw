$( function() {
        $( "#accordion" ).accordion({
            active: false,
            collapsible: true,
            header: "h3",
            heightStyle:"content",
            icons: { header: "ui-icon-plus", activeHeader: "ui-icon-minus" }
        });
    } );

function changeClass(){
    var el = document.getElementById("body");
    if(el.classList.contains("light")) {
        el.className = "dark";
        var all = document.getElementsByClassName('ui-accordion-header');
        for (var i = 0; i < all.length; i++) {
        all[i].style.border = '1px solid #ffcc00';
        }
        document.getElementById("hai").style.color = "#ffcc00";
    }
    else {
        el.className = "light";
        var all = document.getElementsByClassName('ui-accordion-header');
        for (var i = 0; i < all.length; i++) {
        all[i].style.border = '1px solid #042427';
        }
        document.getElementById("hai").style.color = "#042427";
    }
}