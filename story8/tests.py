from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import index8, ambil_buku

# Create your tests here.
class Story8UnitTest(TestCase):
    def test_story8_url_is_exist(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 200)
    
    def test_story8_url_search_is_exist(self):
        response = Client().get('/story8/get/potter')
        self.assertEqual(response.status_code, 200)

    def test_story8_using_index_func(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, index8)

    def test_story8_search_using_views_func(self):
        found = resolve('/story8/get/potter')
        self.assertEqual(found.func, ambil_buku)

    def test_story8_using_index_template(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'story8index.html')