from django.urls import path
from .views import index8, ambil_buku

app_name = 'story8'

urlpatterns = [
    path('', index8, name='index8'),
    path('get/<judul>', ambil_buku)
]