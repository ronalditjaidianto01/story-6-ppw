from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import requests

# Create your views here.
def index8(request):
    r = requests.get('https://www.googleapis.com/books/v1/volumes?q=dog%20days')
    context = {'result':r.json()}
    return render(request, 'story8index.html', context)

def ambil_buku(request, judul):
    r = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + judul)
    return JsonResponse(r.json())