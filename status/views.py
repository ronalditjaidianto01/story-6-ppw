from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import StatusForm
from .models import Status
# Create your views here.

def index(request):
    if request.method == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('/story6/')

    else:
        form = StatusForm()
        list_status = Status.objects.all().order_by('-time')
        context = {'form':form, 'list_status':list_status}
        return render(request, 'index.html', context)