from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import index
from .models import Status
import time
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager

# Create your tests here.
class StatusUnitCase(TestCase):
    def test_status_url_is_exist(self):
        response = Client().get('/story6/')
        self.assertEqual(response.status_code, 200)

    def test_status_using_index_template(self):
        response = Client().get('/story6/')
        self.assertTemplateUsed(response, 'index.html')

    def test_status_using_index_func(self):
        found = resolve('/story6/')
        self.assertEqual(found.func, index)

    def test_new_obj_added_to_models(self):
        Status.objects.create(status="Baik")
        count = Status.objects.all().count()
        self.assertEqual(count,1)

    def test_status_can_save_a_POST_request(self):
        data = {'status':'Baik'}
        response = self.client.post('/story6/', data)
        count = Status.objects.all().count()
        self.assertEqual(count, 1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/story6/')

        new_response = self.client.get('/story6/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Baik', html_response)

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(ChromeDriverManager().install(), chrome_options=chrome_options)

    def tearDown(self):
        self.browser.quit()

    def test_can_submit_status_and_is_shown_in_page(self):
        self.browser.get(self.live_server_url + '/story6/')
        time.sleep(2)
        form = self.browser.find_element_by_id('id_status')
        form.send_keys('baiq')
        form.submit()
        time.sleep(2)
        self.assertIn("baiq",self.browser.page_source)
