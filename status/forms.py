from django import forms
from django.forms import ModelForm
from .models import Status

class StatusForm(ModelForm):
    class Meta:
        model = Status
        fields = ['status']
        widgets = { 
            'status': forms.TextInput(attrs={
                'placeholder':'Ketik disini :)'
            }),
        }