from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import index9, register
from django.contrib.auth.models import User
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
# Create your tests here.
class Story9UnitTest(TestCase):
    def test_story9_url_is_exist(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code, 200)

    def test_story9_using_index_func(self):
        found = resolve('/story9/')
        self.assertEqual(found.func, index9)

    def test_story9_landing_page_using_template(self):
        response = Client().get('/story9/')
        self.assertTemplateUsed(response, 'story9landingpage.html')
        self.assertNotIn('_auth_user_id', self.client.session)

    def test_login_url_exist(self):
        response = Client().get('/story9/auth/login/')
        self.assertEqual(response.status_code, 200)

    def test_logout_url_is_exist(self):
        response = Client().get('/story9/auth/logout/')
        self.assertEqual(response.status_code, 200)
    
    def test_register_url_is_exist(self):
        response = Client().get('/story9/register/')
        self.assertEqual(response.status_code, 200)

    def test_register_using_view_func(self):
        found = resolve('/story9/register/')
        self.assertEqual(found.func, register)

    def test__session_id_exist_when_logged_in(self):
        user = User.objects.create_user('myusername', 'myemail@crazymail.com', 'mypassword')
        user.first_name = 'John'
        user.save()

        response = self.client.post('/story9/auth/login/', data={'username':'myusername', 'password':'mypassword'})
        self.assertEqual(response.status_code, 302)
        self.assertIn('_auth_user_id', self.client.session)

    def test_can_save_user_registration(self):
        count = User.objects.all().count()
        self.assertEqual(count, 0)

        data = {'username':'apaja', 'first_name':'Apaja', 'password1':'hahehihohu', 'password2':'hahehihohu'}
        response = self.client.post('/story9/register/', data)
        count = User.objects.all().count()
        self.assertEqual(count, 1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/story9/')

        new_response = self.client.get('/story9/')
        self.assertTemplateUsed(new_response, 'story9index.html')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Apaja', html_response)