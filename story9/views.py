from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import RegForm
from django.contrib.auth import login, authenticate

# Create your views here.
def index9(request):
    if request.user.is_authenticated:
        context = {'user': request.user}
        return render(request, 'story9index.html', context)
    else:
        return render(request, 'story9landingpage.html')

def register(request):
    if request.user.is_authenticated:
        return redirect('story9:index9')
    else:
        if request.method == "POST":
            form = RegForm(request.POST)
            if form.is_valid():
                form.save()
                username = form.cleaned_data.get('username')
                raw_password = form.cleaned_data.get('password1')
                user = authenticate(username=username, password=raw_password)
                login(request, user)
                return redirect('story9:index9')
        else:
            form = RegForm()
        return render(request, 'registration/regPage.html', {'form': form})

