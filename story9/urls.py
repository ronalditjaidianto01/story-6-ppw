from django.urls import path, include
from .views import index9, register

app_name = 'story9'

urlpatterns = [
    path('', index9, name='index9'),
    path('auth/', include('django.contrib.auth.urls')),
    path('register/', register, name='register'),
]