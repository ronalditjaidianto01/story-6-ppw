from django.urls import path, include
from .views import index10

app_name = 'story10'

urlpatterns = [
    path('', index10, name='index10')
]