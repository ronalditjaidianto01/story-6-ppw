from django.urls import path
from . import views

app_name = 'beranda'

urlpatterns = [
    path('', views.home, name='index'),
]
